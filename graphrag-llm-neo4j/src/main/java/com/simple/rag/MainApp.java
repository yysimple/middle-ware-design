package com.simple.rag;

/**
 * 项目: middle-ware-design
 * <p>
 * 功能描述:
 *
 * @author: WuChengXing
 * @create: 2025-02-14 00:44
 **/
public class MainApp {

    public static void main(String[] args) {
        // 配置 Neo4j 连接信息
        String uri = "bolt://localhost:7687";
        String user = "neo4j";
        String password = "970412@wcx.com";

        // 配置文心一言 API 凭证
        String apiKey = "your_api_key";
        String secretKey = "your_secret_key";

        // 创建 Neo4j 检索器
        Neo4jRetriever retriever = new Neo4jRetriever(uri, user, password);
        // 创建文心一言 LLM 服务
        WenxinYiyanLLM llm = new WenxinYiyanLLM(apiKey, secretKey);
        // 创建 GraphRAG 服务
        GraphRAGService graphRAGService = new GraphRAGService(retriever, llm);

        // 提出问题
        String question = "example_keyword";
        String answer = graphRAGService.answerQuestion(question);
        System.out.println("问题: " + question);
        System.out.println("答案: " + answer);

        // 关闭连接
        retriever.close();
    }
}
