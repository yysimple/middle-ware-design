package com.simple.rag;

import java.util.List;

/**
 * 项目: middle-ware-design
 * <p>
 * 功能描述:
 *
 * @author: WuChengXing
 * @create: 2025-02-14 00:46
 **/
public class GraphRAGService {
    private final Neo4jRetriever retriever;
    private final WenxinYiyanLLM llm;

    public GraphRAGService(Neo4jRetriever retriever, WenxinYiyanLLM llm) {
        this.retriever = retriever;
        this.llm = llm;
    }

    public String answerQuestion(String question) {
        List<String> relatedInfo = retriever.retrieveRelatedInfo(question);
        StringBuilder context = new StringBuilder();
        for (String info : relatedInfo) {
            context.append(info).append(" ");
        }
        return llm.generateAnswer(context.toString(), question);
    }
}
