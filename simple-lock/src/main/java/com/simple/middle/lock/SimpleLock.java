package com.simple.middle.lock;

import java.util.List;
import java.util.concurrent.locks.Lock;

/**
 * 项目: whitelist-spring-boot-starter
 *
 * 功能描述: 顶级锁接口
 *
 * @author: WuChengXing
 *
 * @create: 2023-04-29 00:21
 **/
public interface SimpleLock extends Lock {

    /**
     * 当前所有的分布式锁占用信息
     *
     * @return
     */
    List<String> currentLockKeys();

    /**
     * 尝试获取锁，只带期望过期时间，会返回对应的value给你
     *
     * @param lockKey 锁的key
     * @param expireSeconds 过期时间的秒
     * @return
     */
    String tryLock(String lockKey, long expireSeconds);

    /**
     * 自己指定值的
     *
     * @param lockKey 锁的key
     * @param value 获取锁对应的值
     * @param expireSeconds 过期秒数
     * @return
     */
    boolean tryLock(String lockKey, String value, long expireSeconds);

    /**
     * 尝试着获取锁
     *
     * @param lockKey 锁的key
     * @param expireSeconds 过期秒数
     * @param tryTimes 尝试次数
     * @param sleepMillis 每次尝试睡眠的秒数
     * @return
     */
    String lock(String lockKey, long expireSeconds, int tryTimes, long sleepMillis);

    /**
     * 尝试多次加锁，带循环次数
     *
     * @param lockKey 锁的key
     * @param lockValue 锁的值
     * @param expireSeconds 过期秒数
     * @param tryTimes 尝试次数
     * @param sleepMillis 每次尝试睡眠的秒数
     * @return
     */
    boolean lock(String lockKey, String lockValue, long expireSeconds, int tryTimes, long sleepMillis);

    /**
     * 释放锁
     *
     * @param lockKey 锁的key
     * @param lockValue 锁的值
     */
    void unlock(String lockKey, String lockValue);
}
