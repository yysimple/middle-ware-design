package com.simple.middle.lock.util;

import org.slf4j.helpers.MessageFormatter;

import java.util.UUID;

/**
 * 项目: simple-lock
 *
 * 功能描述:
 *
 * @author: WuChengXing
 *
 * @create: 2023-04-29 00:35
 **/
public class CommonUtils {

    public static String getUuid() {
        return UUID.randomUUID().toString().replace("-", "").toUpperCase();
    }

    public static String format(String message, Object... args) {
        return MessageFormatter.arrayFormat(message, args).getMessage();
    }

}
