package com.simple.middle.attach;

/**
 * 项目: simple-lock
 *
 * 功能描述:
 *
 * @author: WuChengXing
 *
 * @create: 2024-05-10 16:06
 **/
public class SimpleAttachTest {

    public static void main(String[] args) {
        while (true) {
            System.out.println("测试程序 Hello world! --");
            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
