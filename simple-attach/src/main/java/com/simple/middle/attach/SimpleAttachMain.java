package com.simple.middle.attach;

import com.sun.tools.attach.VirtualMachine;

import java.io.IOException;

/**
 * 项目: simple-lock
 *
 * 功能描述: attach启动命令
 *
 * @author: WuChengXing
 *
 * @create: 2024-05-10 16:02
 **/
public class SimpleAttachMain {
    public static void main(String[] args) {
        VirtualMachine vm = null;
        try {
            //MyBizMain进程ID
            vm = VirtualMachine.attach("53708");
            //java agent jar包路径
            vm.loadAgent("/Users/chengxingwu/project/person-project/middle-ware-design/byte-buddy-javaagent/target/VisitMethod.jar");
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (vm != null) {
                try {
                    vm.detach();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
